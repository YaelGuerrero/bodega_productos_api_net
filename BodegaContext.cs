﻿using Microsoft.EntityFrameworkCore;
using Bodega_de_joyas_API.Models;
using System.Threading;

namespace Bodega_de_joyas_API
{
    public class BodegaContext: DbContext
    {
        public DbSet<Categoria> Categorias { get; set; }
        public DbSet<Producto> Productos { get; set; }

        public BodegaContext(DbContextOptions<BodegaContext> options) : base(options) 
        {}

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            List<Categoria> categoriasInit = new List<Categoria>();
            categoriasInit.Add(new Categoria { CategoriaId = Guid.Parse("1f26405e-676b-4ecb-8b73-9e54cb7b6c13"), Nombre = "Aretes", Descripcion = "El articulo de joyería más popular entre las mujeres." });
            categoriasInit.Add(new Categoria { CategoriaId = Guid.Parse("294379ec-7ed9-4b8c-9522-aef13683d412"), Nombre = "Esclavas", Descripcion = "Articulo de joyería usado en la muñeca." });
            categoriasInit.Add(new Categoria { CategoriaId = Guid.Parse("0bf33d1b-8950-4e49-8c96-b2256f3e0333"), Nombre = "Cadenas", Descripcion = "Articulo de joyería usado alrededor del cuello." });

            modelBuilder.Entity<Categoria>(categoria =>
            {
                categoria.ToTable("Categoria");
                categoria.HasKey(p => p.CategoriaId);
                categoria.Property(p => p.Nombre).IsRequired().HasMaxLength(100);
                categoria.Property(p => p.Descripcion).IsRequired(false);
                categoria.HasData(categoriasInit);
            });

            List<Producto> productsInit = new List<Producto>();
            productsInit.Add(new Producto { ProductoId = Guid.Parse("4b6f7966-cb6a-4b6b-b855-a9fa895c3cc9"), CategoriaId = Guid.Parse("1f26405e-676b-4ecb-8b73-9e54cb7b6c13"), Nombre = "Cuadrado del 3", Descripcion = "Arete cuadrado con piedra de 3 milimetros", TipoMetal = TipoMetal.Plata, PesoGramos = 3, Precio = 800 });
            productsInit.Add(new Producto { ProductoId = Guid.Parse("67dc9079-5346-468d-93bb-b61d927f871d"), CategoriaId = Guid.Parse("294379ec-7ed9-4b8c-9522-aef13683d412"), Nombre = "Esclava con grabado", Descripcion = "Esclava con grabado en una placa", TipoMetal = TipoMetal.Oro, PesoGramos = 120, Precio = 239129 });
            productsInit.Add(new Producto { ProductoId = Guid.Parse("7c875f0f-9e7e-4e9a-871c-106c81ec3e2f"), CategoriaId = Guid.Parse("294379ec-7ed9-4b8c-9522-aef13683d412"), Nombre = "Esclava solo eslavones", Descripcion = "Esclava con unicamente eslavones.", TipoMetal = TipoMetal.Laton, PesoGramos = 90, Precio = 370 });
            productsInit.Add(new Producto { ProductoId = Guid.Parse("eb8695ca-1e3f-419e-aa5b-5c44292d5f5d"), CategoriaId = Guid.Parse("0bf33d1b-8950-4e49-8c96-b2256f3e0333"), Nombre = "Cadena cubana", Descripcion = "Cadena con corte cubano.", TipoMetal = TipoMetal.Hacero, PesoGramos = 140, Precio = 280 });

            modelBuilder.Entity<Producto>(producto =>
            {
                producto.ToTable("Producto");
                producto.HasKey(p => p.ProductoId);
                producto.HasOne(p => p.Categoria).WithMany(p => p.Productos).HasForeignKey(p => p.CategoriaId);
                producto.Property(p => p.Nombre).IsRequired().HasMaxLength(100);
                producto.Property(p => p.Descripcion).IsRequired(false);
                producto.Property(p => p.TipoMetal).IsRequired();
                producto.Property(p => p.PesoGramos).IsRequired();
                producto.Property(p => p.Precio).IsRequired();
                producto.HasData(productsInit);
            });
        }
    }
}
