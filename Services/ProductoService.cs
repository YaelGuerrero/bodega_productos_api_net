﻿using Bodega_de_joyas_API.Models;
using Microsoft.AspNetCore.Mvc;

namespace Bodega_de_joyas_API.Services;

public class ProductoService : IProductoService
{
    BodegaContext context;
    public ProductoService(BodegaContext dbcontext)
    {
        context = dbcontext;
    }

    public IEnumerable<Producto> Get()
    {
        return context.Productos;
    }
    public async Task Save(Producto producto)
    {
        producto.ProductoId= Guid.NewGuid();
        await context.AddAsync(producto);
        await context.SaveChangesAsync();
    }
    public async Task Update(Guid id, Producto producto)
    {
        var productoActual = context.Productos.Find(id);
        if (productoActual == null)
        {
            productoActual.Nombre = producto.Nombre;
            productoActual.Descripcion= producto.Descripcion;
            productoActual.TipoMetal = producto.TipoMetal;
            productoActual.PesoGramos = producto.PesoGramos;
            productoActual.CategoriaId = producto.CategoriaId;
            productoActual.Precio = producto.Precio;
            await context.SaveChangesAsync();
        }
    }
    public async Task Delete(Guid id)
    {
        var productoActual = context.Productos.Find(id);
        if (productoActual == null)
        {
            context.Remove(productoActual);
            await context.SaveChangesAsync();
        }
    }
}

public interface IProductoService
{
    IEnumerable<Producto> Get();
    Task Save(Producto producto);
    Task Update(Guid id, Producto producto);
    Task Delete(Guid id);
}