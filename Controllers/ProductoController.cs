﻿using Microsoft.AspNetCore.Mvc;
using Bodega_de_joyas_API.Models;
using Bodega_de_joyas_API.Services;

namespace Bodega_de_joyas_API.Controllers;

[Route("api/[controller]")]
public class ProductoController : ControllerBase
{
    IProductoService productoService;
    public ProductoController (IProductoService productoService)
    {
        this.productoService = productoService;
    }
    [HttpGet]
    public IActionResult Get()
    {
        return Ok(productoService.Get());
    }
    [HttpPost]
    public IActionResult Post([FromBody] Producto producto)
    {
        productoService.Save(producto);
        return Ok();
    }
    [HttpPut("{id}")]
    public IActionResult Put(Guid id,[FromBody] Producto producto) 
    { 
        productoService.Update(id, producto);
        return Ok();
    }
    [HttpDelete("{id}")]
    public IActionResult Delete(Guid id)
    {
        productoService.Delete(id);
        return Ok();
    }
}