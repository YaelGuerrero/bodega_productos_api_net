﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Bodega_de_joyas_API.Controllers;

[ApiController]
[Route("api/[controller]")]
public class DataBaseController : ControllerBase
{
    BodegaContext bodegaContext;
    private readonly ILogger<DataBaseController> _logger;
    public DataBaseController(ILogger<DataBaseController> logger, BodegaContext db)
    {
        _logger = logger;
        bodegaContext = db;
    }
    [HttpGet]
    [Route("createDatabase")]
    public ActionResult CreateDatabase()
    {
        bodegaContext.Database.EnsureCreated();
        return Ok();
    }
}

