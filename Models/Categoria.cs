﻿using System.Text.Json.Serialization;

namespace Bodega_de_joyas_API.Models
{
    public class Categoria
    {
        public Guid CategoriaId { get; set; }
        public string Nombre { get;set; }
        public string Descripcion { get; set; }

        [JsonIgnore]
        public virtual ICollection<Producto> Productos { get; set; }
    }
}
