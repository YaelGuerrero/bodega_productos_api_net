﻿using System.Text.Json.Serialization;

namespace Bodega_de_joyas_API.Models;

public class Producto
{
    public Guid ProductoId { get; set; }
    public Guid CategoriaId { get; set; }
    public string Nombre { get; set; }
    public string Descripcion { get; set; }
    public int PesoGramos { get; set; }
    public TipoMetal TipoMetal { get; set; }
    public int Precio { get; set; }
    [JsonIgnore]
    public virtual Categoria Categoria { get; set; }
}

public enum TipoMetal
{
    Oro,
    Plata,
    Laton,
    Hacero
}